export interface Iaddproduct {
  size:string,
  color:string,
  mrp:number,
  saleprice:number,
  quantity:number,
  offer:number
}


export interface IAddShopProduct {

    shop_id : string,
    product_color : string,
    product_id : string,
    product_size : string,
    qty : number,
    mrp : number,
    selling_price : number,
    discount : number
}
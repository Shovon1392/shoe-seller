import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends RootService {

  constructor(http: HttpClient){
    super(http);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }



  /**
   * Get allmaster product list
   */
  getAllMasterProduct(): Observable<any>{
    return this.http.get(this.master_product,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }


 

  /**
   * End of the class
   */
}

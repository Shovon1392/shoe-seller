import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';
import Amplify, { Auth, Storage } from 'aws-amplify';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RootService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
     //'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  httpOptions = {
    headers: this.headers
  };

  constructor(
    public http: HttpClient
  ) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //  'Authorization': 'Bearer '+ localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }
  timeOutLimit = 1000*15;

  /**
   * Base urls
   */

  base_product_url = 'https://jhk9yt94tl.execute-api.us-east-1.amazonaws.com/dev/api/v1/';
  base_master_url = 'https://7h6jh8qei0.execute-api.us-east-1.amazonaws.com/dev/api/v1/';
  base_seller_url = 'https://upg879bp3f.execute-api.us-east-1.amazonaws.com/dev/api/v1/';
  /**
   * Product
   */

   master_product = this.base_product_url+'product/admin';

   /** Master */
  master_color = this.base_master_url+'master/color';
  seller_details =   this.base_seller_url+'seller';
  shop_product = this.base_seller_url+'shop/product';
  shop_product_list = this.base_seller_url+'shop';
  shop_product_details = this.base_seller_url+'product';


  /**
   * End
   */
}

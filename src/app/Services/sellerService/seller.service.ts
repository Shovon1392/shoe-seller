import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';
import { IAddShopProduct } from 'src/app/Interface/Product/iaddproduct';

@Injectable({
  providedIn: 'root'
})
export class SellerService extends RootService  {

  constructor(http: HttpClient){
    super(http);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }

  /** get seller details by auth id */
  getSellerById(id){
    return this.http.get(this.seller_details+'/'+id,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }

  /** Add shop product  */
  addToShop(data : IAddShopProduct){
    return this.http.post(this.shop_product,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }


  /** get all product list of a particular shop */
  getShopPrroductList(shopId): Observable<any>{
  return this.http.get(this.shop_product_list+'/'+shopId,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }

  /** get all product list of a particular shop */
  getShopProductDetails(productId): Observable<any>{
    return this.http.get(this.shop_product_details+'/'+productId,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
    }
  /** End of class */
}

import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AccountComponent } from './components/account/account.component';
import { AddProductComponent } from './components/products/add-product/add-product.component';
import { AuthComponent } from './components/auth/auth.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductDetailComponent } from './components/products/product-detail/product-detail.component';
import { ProductComponent } from './components/products/product/product.component';
import { DefaultComponent } from './layouts/default/default.component';
import { FullwidthComponent } from './layouts/fullwidth/fullwidth.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch:'full'},
  {
    path : '',
    component : FullwidthComponent,
    children : [{
      path:'login',
      component : AuthComponent,
      }
   ]
  },

  {
    path : '',
    component : DefaultComponent,
    children : [{
      path : 'dashboard',
      component : DashboardComponent
     }
     , {
       path : 'product',
       component : ProductComponent
     },
     {
      path : 'add-product',
      component : AddProductComponent
     },
     {
      path : 'product-details/:productId',
      component : ProductDetailComponent
     },
     {
       path : 'myAccount',
       component : AccountComponent
     }
    ]
   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import {ProductService } from '../Services/productService/product.service';
import { SellerService } from '../Services/sellerService/seller.service';
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  constructor(
    public activateRoute : ActivatedRoute,
    public router: Router,
    public spinner: NgxSpinnerService,

    /**Custom services */
    public productService: ProductService,
    public sellerService : SellerService,
  ) { }

  ngOnInit(): void {
  }

  sellerDetails = JSON.parse(localStorage.getItem('shopDetails') );
  /**
   * Custom log
   */
  log(data){
    console.log(data);
  }


/** get random images */
getRandomImages(images){
  let noImage = 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg';

  //console.log(images)
  if(images.length > 0 ){
    // setInterval(function(){ 
      let randomNo = Math.floor(Math.random() * (images.length)); 
      console.log(randomNo)
      return images[randomNo]['thumb_image'];
    // }, 10000);
  } else {
    return noImage;
  }
}
  /**
   * End of classs
   */
}

import { Component, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';
import { BaseComponent } from 'src/app/base/base.component';
import {ILogin} from '../../Interface/Auth/ilogin';
import {ISignup} from '../../Interface/Auth/isignup';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent extends BaseComponent implements OnInit {

  currentStep = 'login';
  confirmUser : any;
  confirmCode : any;

  loginData : ILogin = {
    email: '',
    password: ''
  }

  signUpData : ISignup = {
    email: '',
    password: '',
    name: '',
    phone: ''
  }


  ngOnInit(): void {
  }


/** login for registerd user */
  async login(){
    console.log(this.loginData);
    if(this.loginData.email && this.loginData.password){
      try {
        this.spinner.show();
        let username = this.loginData.email;
        let password = this.loginData.password;
        const user = await Auth.signIn(username, password).then(
          res =>{
            console.log(res)
            console.log(res.username)
            this.spinner.hide();
            if(res.username){
              this.getSellerDetails(res.username)
              
            }
          }, err=>{
            console.log(err);
            this.spinner.hide();
            if(err.code == "NotAuthorizedException"){
              alert(err['message'])
            } else if(err.code == "UserNotConfirmedException"){
              alert(err['message']);
              this.confirmUser = this.loginData.email;
              this.currentStep = 'confirmSignup';
            }
          }
        );
    } catch (error) {
      this.spinner.hide();
        console.log('error signing in', error);
    }
    }else{
      alert('Please enter valid email address!')
    }
  }


  /** Sign up */
  signup(){
    let username = this.signUpData.email;
    let password = this.signUpData.password;
    console.log(this.signUpData);
    this.spinner.show();
    if(this.signUpData){
      Auth.signUp({
        username,
        password,
        attributes: {
            name: this.signUpData.name,
            email: this.signUpData.email,          // optional
            phone_number: '+91'+this.signUpData.phone,
        }
    }).then(
      res => {
          this.spinner.hide();
          if(!res.userConfirmed ){
            this.confirmUser = this.signUpData.email;
              this.currentStep = 'confirmSignup';
            //this.confirmOTP(username);
          }
          console.log(res)
        }, err=>{
          this.spinner.hide();
          if(err['code'] == 'UsernameExistsException'){
            //this.confirmOTP(username)
          }else{
            alert(err.message)
          }

          console.log(err)
        }
    )
    }
  }


  /**confirm user */
  async confirm(){
    let confirm = await Auth.confirmSignUp(this.confirmUser, this.confirmCode).then(
      res => {
        // this.spinner.hide();
        if(res == 'SUCCESS'){
          this.currentStep = 'login';
        }
        console.log(res)
      }, err=>{
        this.spinner.hide();
        // if(err['code'] == 'UsernameExistsException'){
        //   //this.confirmOTP(username)
        // }else{
        //   alert(err.message)
        // }

        console.log(err)
      }
    )
  }
  /** Change step */
  changeStep(step){
    this.currentStep = step;
  }

  /** Get seller details by user */
  getSellerDetails(authId){

    this.sellerService.getSellerById(authId).subscribe(
      res =>{
        if(res['data'].length > 0){
          localStorage.setItem('shopDetails' , JSON.stringify(res['data'][0][0]));
          this.router.navigateByUrl('dashboard');
        }
        
      }
    )
    
  }
    /**
   * End of class
   *
   */
}

import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { IAddShopProduct } from 'src/app/Interface/Product/iaddproduct';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent extends BaseComponent implements OnInit {
	allProduct: any = [];
	selectedProduct: any = [];
	addProductData : IAddShopProduct ={
		shop_id : this.sellerDetails.shopId,
		product_color : '',
		product_id : '',
		product_size : '',
		qty : 1,
		mrp : 0.00,
		discount : 0.00,
		selling_price : 0.00 
	}
	ngOnInit(): void {
		this.getAllProduct();
	}
	/**Set selected product */
	selectProduct(product) {
		this.selectedProduct = product;
		
	}
	/**
	 * GEt all products
	 */
	getAllProduct() {
		try {
			this.allProduct = [];
			this.spinner.show()
			this.productService.getAllMasterProduct().subscribe(res => {
				this.spinner.hide();
				if (res['data'][0].length > 0) {
					res['data'][0].forEach(element => {
						let tempdata = (element);
						tempdata.colors = (tempdata.colors) ? JSON.parse("[" + JSON.parse(tempdata.colors)[0] + "]") : [];
						tempdata.sizes = (tempdata.sizes) ? JSON.parse("[" + JSON.parse(tempdata.sizes) + "]") : [];
						tempdata.images = (tempdata.images) ? JSON.parse("[" + JSON.parse(tempdata.images) + "]") : [];
						if(tempdata.colors.length > 0 && tempdata.sizes.length > 0){
							this.allProduct.push(tempdata);
						}
					});
					console.log(this.allProduct)
				}
			}, err => {
				this.spinner.hide();
				console.log(err)
			})
		} catch (error) {
			this.spinner.hide();
		}
	}

	/** Save shop product  */
	addShopProduct(){
		try {
			this.spinner.show();
			this.addProductData['product_id'] = this.selectedProduct['productId'];
			console.log(this.selectedProduct)
			console.log(this.addProductData);
			this.sellerService.addToShop(this.addProductData).subscribe(res => {
				this.spinner.hide();
				this.selectedProduct = [];
				document.getElementById('productAddModal').click();
			}, err => {
				this.spinner.hide();
				console.log(err)
			})
		} catch (error) {
			this.spinner.hide();
		}
	}
	/** End of class */
}
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { Iaddproduct } from 'src/app/Interface/Product/iaddproduct';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent extends BaseComponent implements OnInit {

  addProductData : Iaddproduct[]=[{
     "size" : null,
     "color" : null,
      "mrp" : null,
      "saleprice" : null,
      "quantity" : null,
      "offer" : null
  }];
productId: any;
productDetails : any =[]; 

  ngOnInit(): void {
    this.activateRoute.params.subscribe(params => {
      console.log(params.productId)
      this.productId = params.productId;
      this.getProductDetails(params.productId );
    });
  }

  /* Add product size */
  addProductSize()
  {
        this.addProductData.push(
            {
              "size" : null,
              "color" : null,
               "mrp" : null,
               "saleprice" : null,
               "quantity" : null,
               "offer" : null
            })
  }

  /* Delete product size */
  deleteProductSize(plist)
  {
     this.addProductData.splice(this.addProductData.indexOf(plist),1);
  }

  sliderScroll(dir:number)
  {
    if(dir==1)
     document.querySelector('.slider').scrollLeft -= 100; //scroll to left
    else
    document.querySelector('.slider').scrollLeft += 100; //scroll to right
  }

/**
 * GEt  product details
 */
getProductDetails(productId) {
  try {
    this.productDetails = [];
    this.spinner.show()
    this.sellerService.getShopProductDetails(productId).subscribe(res => {
      this.spinner.hide();
      // if (res['data'][0].length > 0) {
      //   res['data'][0].forEach(element => {
      //     let tempdata = (element);
      //     tempdata.colors = (tempdata.colors) ? JSON.parse("[" + JSON.parse(tempdata.colors)[0] + "]") : [];
      //     tempdata.sizes = (tempdata.sizes) ? JSON.parse("[" + JSON.parse(tempdata.sizes) + "]") : [];
      //     tempdata.images = (tempdata.images) ? JSON.parse("[" + JSON.parse(tempdata.images) + "]") : [];
      //     tempdata.products = (tempdata.products) ? JSON.parse("[" + JSON.parse(tempdata.products) + "]") : [];
          
      //   });
        
      // }
      this.productDetails = res['data'][0][0];
      this.productDetails.available_colors = ( this.productDetails.available_colors) ? JSON.parse("[" + JSON.parse( this.productDetails.available_colors)[0] + "]") : [];
      this.productDetails.available_sizes = ( this.productDetails.available_sizes) ? JSON.parse("[" + JSON.parse( this.productDetails.available_sizes)[0] + "]") : [];
      this.productDetails.images = ( this.productDetails.images) ? JSON.parse("[" + JSON.parse( this.productDetails.images)[0] + "]") : [];
      // this.productDetails.available_colors = ( this.productDetails.available_colors) ? JSON.parse("[" + JSON.parse( this.productDetails.available_colors)[0] + "]") : [];
      console.log(this.productDetails)
    }, err => {
      this.spinner.hide();
      console.log(err)
    })
  } catch (error) {
    this.spinner.hide();
  }
}


getProductDetailsById(id){
  this.router.navigateByUrl('product-details/'+id);
}
  /** End of the class */
}

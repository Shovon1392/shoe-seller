import { Component, OnInit } from '@angular/core';

import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent extends BaseComponent implements OnInit {



shopProduct: any = [];

ngOnInit(): void {
  this.getAllshopProduct();
}

/**
 * GEt all products
 */
	getAllshopProduct() {
		try {
			this.shopProduct = [];
			this.spinner.show()
			this.sellerService.getShopPrroductList(this.sellerDetails['shopId']).subscribe(res => {
				this.spinner.hide();
				if (res['data'][0].length > 0) {
					res['data'][0].forEach(element => {
						let tempdata = (element);
						tempdata.colors = (tempdata.colors) ? JSON.parse("[" + JSON.parse(tempdata.colors)[0] + "]") : [];
						tempdata.sizes = (tempdata.sizes) ? JSON.parse("[" + JSON.parse(tempdata.sizes) + "]") : [];
            tempdata.images = (tempdata.images) ? JSON.parse("[" + JSON.parse(tempdata.images) + "]") : [];
            tempdata.products = (tempdata.products) ? JSON.parse("[" + JSON.parse(tempdata.products) + "]") : [];
            

            
						if(tempdata.colors.length > 0 && tempdata.sizes.length > 0){
							this.shopProduct.push(tempdata);
						}
					});
					console.log(this.shopProduct)
				}
			}, err => {
				this.spinner.hide();
				console.log(err)
			})
		} catch (error) {
			this.spinner.hide();
		}
  }
  
/** Get product details of initial shop product  */

getProductDetails(shopProduct){
  this.router.navigateByUrl('product-details/'+shopProduct[0]['productId']);
}

/** End of class */
}

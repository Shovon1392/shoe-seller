import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent} from './default.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxSpinnerModule } from 'ngx-spinner';


import { ProductComponent } from 'src/app/components/products/product/product.component';
import { AccountComponent } from 'src/app/components/account/account.component';
import { AddProductComponent} from 'src/app/components/products/add-product/add-product.component';
import { ProductDetailComponent} from 'src/app/components/products/product-detail/product-detail.component'



@NgModule({
  declarations: [
     DefaultComponent,
     ProductComponent,
     AddProductComponent,
     ProductDetailComponent,
     AccountComponent
  ],
  imports: [
    SharedModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    NgxSpinnerModule,
  ]
})
export class DefaultModule { }

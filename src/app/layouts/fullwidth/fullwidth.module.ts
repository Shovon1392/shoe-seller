import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullwidthComponent } from './fullwidth.component'
import { AuthComponent} from '../../components/auth/auth.component'
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser'


@NgModule({
  declarations: [
    FullwidthComponent,
    AuthComponent
  ],
  imports: [
    FormsModule,
    RouterModule,
    NgxSpinnerModule,
    BrowserModule 
  ]
})
export class FullwidthModule { }
